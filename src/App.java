import javax.sound.midi.Receiver;
import javax.swing.plaf.synth.SynthSplitPaneUI;

import com.devcamp.Rectangle;
import com.devcamp.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle("Green", 2, 5);
        Rectangle rectangle2 = new Rectangle("Red", 5, 9);
        System.out.println(rectangle1.toString());

        Triangle triangle1 = new Triangle("Blue", 5, 8);
        Triangle triangle2 = new Triangle("Organge", 7, 3);
        System.out.println(triangle1.toString());

        System.out.println("Dien tich Rectangle1: " + rectangle1.getArea());

        System.out.println("Dien tich Triangle1: " + triangle1.getArea());

    }
}
